# Revision 4
## Changes from Revision 3

TODO
- [ ] Replace fan screw connecter with smaller one
- [ ] Fix issues
  - [ ] Supply
  - [ ] enable -8V by default
  - [ ] fix 74HCT123
  - [ ] disable direct voltage check for work voltage, hook it to the
        switch
- [ ] make switch polarity adjustable on PCB
- [ ] Rework the power supply stage
- [ ] Use VVA instead of mixer (?)


# Revision 3
## Issues
* Negative power rail makes no sense and shortens transformer. Cut trace
  and supply externally for testing
* logic direction of the enable switch seems inverted (on is bottom; best
  to add an option to flip it on the PCB as we might want to have it this
  way when combining it with the DDS)
* RCext and Cext of 74HCT123 are flipped on the B unit. Fix by soldering
  R26 rotated by 90 degrees
* the -8V supply should always be enabled, independent of the state of the
  sequencer. cut trace to enable pin and added a wire to +5V
* LM2901 unit C for the gate work voltage is connected to negative
  voltages, but supply is only +5V. Cut the feedback to LM2901 as it
  clamps the negative voltage to -1V and desolder R28 & R34. Then connect
  the enable of +8V to the gate voltage enable

## Changes from Revision 2
List is not complete as it was created later on
* Changed the sequencer to a solution using discrete logic circuits as we
  did not manage to get the sequencer to work properly
* Changed the power supplies from half-bridge to full-bridge to use the
  full current potential of the transformers. Messed up (see issues above)
