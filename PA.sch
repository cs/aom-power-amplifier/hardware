EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 9
Title "AOM Power Amplifier"
Date ""
Rev "1"
Comp "LMU"
Comment1 "Hendrik v. Raven"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L AOMPowerAmp-rescue:HMC1099PM5E-RF_Amplifier U29
U 1 1 5D360D0E
P 6950 5200
F 0 "U29" H 7050 5567 50  0000 C CNN
F 1 "HMC1099PM5E" H 7050 5476 50  0000 C CNN
F 2 "Package_CSP:LFCSP-32-1EP_5x5mm_P0.5mm_EP3.1x3.1mm" H 7000 5200 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/HMC1099PM5E.pdf" H 7000 5200 50  0001 C CNN
F 4 "Analog" H -1350 -300 50  0001 C CNN "MFR"
F 5 "HMC1099PM5E" H -1350 -300 50  0001 C CNN "MPN"
F 6 "Mouser" H -1350 -300 50  0001 C CNN "SPR"
F 7 "584-HMC1099PM5E" H -1350 -300 50  0001 C CNN "SPN"
F 8 "-" H -1350 -300 50  0001 C CNN "SPURL"
	1    6950 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:L L6
U 1 1 5D362573
P 8050 5200
F 0 "L6" V 7869 5200 50  0000 C CNN
F 1 "5n6" V 7960 5200 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 8050 5200 50  0001 C CNN
F 3 "~" H 8050 5200 50  0001 C CNN
F 4 "Murata" H -1350 -300 50  0001 C CNN "MFR"
F 5 "LQW15AN5N6C10D" H -1350 -300 50  0001 C CNN "MPN"
F 6 "RS" H -1350 -300 50  0001 C CNN "SPR"
F 7 "792-6460P" H -1350 -300 50  0001 C CNN "SPN"
F 8 "-" H -1350 -300 50  0001 C CNN "SPURL"
	1    8050 5200
	0    1    1    0   
$EndComp
$Comp
L Device:L L5
U 1 1 5D3631B9
P 6150 5200
F 0 "L5" V 5969 5200 50  0000 C CNN
F 1 "5n6" V 6060 5200 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 6150 5200 50  0001 C CNN
F 3 "~" H 6150 5200 50  0001 C CNN
F 4 "Murata" H -1350 -300 50  0001 C CNN "MFR"
F 5 "LQW15AN5N6C10D" H -1350 -300 50  0001 C CNN "MPN"
F 6 "RS" H -1350 -300 50  0001 C CNN "SPR"
F 7 "792-6460P" H -1350 -300 50  0001 C CNN "SPN"
F 8 "-" H -1350 -300 50  0001 C CNN "SPURL"
	1    6150 5200
	0    1    1    0   
$EndComp
$Comp
L Device:L L4
U 1 1 5D363CD7
P 7800 4850
F 0 "L4" H 7757 4804 50  0000 R CNN
F 1 "0.9u 1A" H 7757 4895 50  0000 R CNN
F 2 "Inductor_SMD:L_1008_2520Metric" H 7800 4850 50  0001 C CNN
F 3 "https://ecsxtal.com/store/pdf/ECS-MPI2520-SMD-POWER-INDUCTOR.pdf" H 7800 4850 50  0001 C CNN
F 4 "ECS" H -1350 -300 50  0001 C CNN "MFR"
F 5 "ECS-MPI2520R0-1R0-R" H -1350 -300 50  0001 C CNN "MPN"
F 6 "Digi-Key" H -1350 -300 50  0001 C CNN "SPR"
F 7 "XC2410CT-ND" H -1350 -300 50  0001 C CNN "SPN"
F 8 "-" H -1350 -300 50  0001 C CNN "SPURL"
	1    7800 4850
	-1   0    0    1   
$EndComp
$Comp
L Device:R R58
U 1 1 5D3645CF
P 6400 4850
F 0 "R58" H 6470 4896 50  0000 L CNN
F 1 "68.1" H 6470 4805 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6330 4850 50  0001 C CNN
F 3 "~" H 6400 4850 50  0001 C CNN
F 4 "RND" H -1350 -300 50  0001 C CNN "MFR"
F 5 "1550402WGF680JTCE" H -1350 -300 50  0001 C CNN "MPN"
F 6 "Distrelec" H -1350 -300 50  0001 C CNN "SPR"
F 7 "300-56-446" H -1350 -300 50  0001 C CNN "SPN"
F 8 "-" H -1350 -300 50  0001 C CNN "SPURL"
	1    6400 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C78
U 1 1 5D365554
P 5900 5450
F 0 "C78" H 6015 5496 50  0000 L CNN
F 1 "3p3" H 6015 5405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5938 5300 50  0001 C CNN
F 3 "~" H 5900 5450 50  0001 C CNN
F 4 "Wurth Elektronik" H -1350 -300 50  0001 C CNN "MFR"
F 5 "885012005052" H -1350 -300 50  0001 C CNN "MPN"
F 6 "Distrelec" H -1350 -300 50  0001 C CNN "SPR"
F 7 "300-67-364" H -1350 -300 50  0001 C CNN "SPN"
F 8 "-" H -1350 -300 50  0001 C CNN "SPURL"
	1    5900 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C79
U 1 1 5D365920
P 8300 5450
F 0 "C79" H 8415 5496 50  0000 L CNN
F 1 "3p3" H 8415 5405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8338 5300 50  0001 C CNN
F 3 "~" H 8300 5450 50  0001 C CNN
F 4 "Wurth Elektronik" H -1350 -300 50  0001 C CNN "MFR"
F 5 "885012005052" H -1350 -300 50  0001 C CNN "MPN"
F 6 "Distrelec" H -1350 -300 50  0001 C CNN "SPR"
F 7 "300-67-364" H -1350 -300 50  0001 C CNN "SPN"
F 8 "-" H -1350 -300 50  0001 C CNN "SPURL"
	1    8300 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C76
U 1 1 5D366527
P 5650 5200
F 0 "C76" V 5398 5200 50  0000 C CNN
F 1 "2n2" V 5489 5200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5688 5050 50  0001 C CNN
F 3 "~" H 5650 5200 50  0001 C CNN
F 4 "Wurth Elektronik" H -1350 -300 50  0001 C CNN "MFR"
F 5 "885012205063" H -1350 -300 50  0001 C CNN "MPN"
F 6 "Distrelec" H -1350 -300 50  0001 C CNN "SPR"
F 7 "300-67-756" H -1350 -300 50  0001 C CNN "SPN"
F 8 "-" H -1350 -300 50  0001 C CNN "SPURL"
	1    5650 5200
	0    1    1    0   
$EndComp
$Comp
L Device:C C77
U 1 1 5D366943
P 8550 5200
F 0 "C77" V 8298 5200 50  0000 C CNN
F 1 "2n2" V 8389 5200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8588 5050 50  0001 C CNN
F 3 "~" H 8550 5200 50  0001 C CNN
F 4 "Wurth Elektronik" H -1350 -300 50  0001 C CNN "MFR"
F 5 "885012205063" H -1350 -300 50  0001 C CNN "MPN"
F 6 "Distrelec" H -1350 -300 50  0001 C CNN "SPR"
F 7 "300-67-756" H -1350 -300 50  0001 C CNN "SPN"
F 8 "-" H -1350 -300 50  0001 C CNN "SPURL"
	1    8550 5200
	0    1    1    0   
$EndComp
$Comp
L Device:C C72
U 1 1 5D367E88
P 5900 4000
F 0 "C72" H 6015 4046 50  0000 L CNN
F 1 "2n2" H 6015 3955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5938 3850 50  0001 C CNN
F 3 "~" H 5900 4000 50  0001 C CNN
F 4 "Wurth Elektronik" H -1350 -600 50  0001 C CNN "MFR"
F 5 "885012205063" H -1350 -600 50  0001 C CNN "MPN"
F 6 "Distrelec" H -1350 -600 50  0001 C CNN "SPR"
F 7 "300-67-756" H -1350 -600 50  0001 C CNN "SPN"
F 8 "-" H -1350 -600 50  0001 C CNN "SPURL"
	1    5900 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C71
U 1 1 5D36874D
P 5400 4000
F 0 "C71" H 5515 4046 50  0000 L CNN
F 1 "10u" H 5515 3955 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 5438 3850 50  0001 C CNN
F 3 "~" H 5400 4000 50  0001 C CNN
F 4 "Taiyo Yuden" H -1350 -600 50  0001 C CNN "MFR"
F 5 "UMK325AB7106KMHP" H -1350 -600 50  0001 C CNN "MPN"
F 6 "RS" H -1350 -600 50  0001 C CNN "SPR"
F 7 "184-3643P" H -1350 -600 50  0001 C CNN "SPN"
F 8 "-" H -1350 -600 50  0001 C CNN "SPURL"
	1    5400 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C70
U 1 1 5D368AAC
P 4950 4000
F 0 "C70" H 5065 4046 50  0000 L CNN
F 1 "10u" H 5065 3955 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 4988 3850 50  0001 C CNN
F 3 "~" H 4950 4000 50  0001 C CNN
F 4 "Taiyo Yuden" H -1350 -600 50  0001 C CNN "MFR"
F 5 "UMK325AB7106KMHP" H -1350 -600 50  0001 C CNN "MPN"
F 6 "RS" H -1350 -600 50  0001 C CNN "SPR"
F 7 "184-3643P" H -1350 -600 50  0001 C CNN "SPN"
F 8 "-" H -1350 -600 50  0001 C CNN "SPURL"
	1    4950 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C73
U 1 1 5D36B153
P 8300 4000
F 0 "C73" H 8415 4046 50  0000 L CNN
F 1 "2n2" H 8415 3955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8338 3850 50  0001 C CNN
F 3 "~" H 8300 4000 50  0001 C CNN
F 4 "Wurth Elektronik" H -1350 -600 50  0001 C CNN "MFR"
F 5 "885012205063" H -1350 -600 50  0001 C CNN "MPN"
F 6 "Distrelec" H -1350 -600 50  0001 C CNN "SPR"
F 7 "300-67-756" H -1350 -600 50  0001 C CNN "SPN"
F 8 "-" H -1350 -600 50  0001 C CNN "SPURL"
	1    8300 4000
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C74
U 1 1 5D36B15D
P 8800 4000
F 0 "C74" H 8915 4046 50  0000 L CNN
F 1 "10u" H 8915 3955 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 8838 3850 50  0001 C CNN
F 3 "~" H 8800 4000 50  0001 C CNN
F 4 "Taiyo Yuden" H -1350 -600 50  0001 C CNN "MFR"
F 5 "UMK325AB7106KMHP" H -1350 -600 50  0001 C CNN "MPN"
F 6 "RS" H -1350 -600 50  0001 C CNN "SPR"
F 7 "184-3643P" H -1350 -600 50  0001 C CNN "SPN"
F 8 "-" H -1350 -600 50  0001 C CNN "SPURL"
	1    8800 4000
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C75
U 1 1 5D36B167
P 9250 4000
F 0 "C75" H 9365 4046 50  0000 L CNN
F 1 "10u" H 9365 3955 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 9288 3850 50  0001 C CNN
F 3 "~" H 9250 4000 50  0001 C CNN
F 4 "Taiyo Yuden" H -1350 -600 50  0001 C CNN "MFR"
F 5 "UMK325AB7106KMHP" H -1350 -600 50  0001 C CNN "MPN"
F 6 "RS" H -1350 -600 50  0001 C CNN "SPR"
F 7 "184-3643P" H -1350 -600 50  0001 C CNN "SPN"
F 8 "-" H -1350 -600 50  0001 C CNN "SPURL"
	1    9250 4000
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5D36B179
P 9250 4300
F 0 "#PWR0101" H 9250 4050 50  0001 C CNN
F 1 "GND" H 9350 4150 50  0000 R CNN
F 2 "" H 9250 4300 50  0001 C CNN
F 3 "" H 9250 4300 50  0001 C CNN
	1    9250 4300
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5D37055D
P 6950 5700
F 0 "#PWR0104" H 6950 5450 50  0001 C CNN
F 1 "GND" H 6955 5527 50  0000 C CNN
F 2 "" H 6950 5700 50  0001 C CNN
F 3 "" H 6950 5700 50  0001 C CNN
	1    6950 5700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5D371F00
P 8300 5700
F 0 "#PWR0105" H 8300 5450 50  0001 C CNN
F 1 "GND" H 8305 5527 50  0000 C CNN
F 2 "" H 8300 5700 50  0001 C CNN
F 3 "" H 8300 5700 50  0001 C CNN
	1    8300 5700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5D372A9F
P 5900 5700
F 0 "#PWR0103" H 5900 5450 50  0001 C CNN
F 1 "GND" H 5905 5527 50  0000 C CNN
F 2 "" H 5900 5700 50  0001 C CNN
F 3 "" H 5900 5700 50  0001 C CNN
	1    5900 5700
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Heatsink_Pad HS3
U 1 1 5D455585
P 7300 5400
F 0 "HS3" H 7441 5439 50  0000 L CNN
F 1 "ICK S 40x40x20" H 7441 5348 50  0000 L CNN
F 2 "AOMPowerAmp:Heatsink_40x40mm_Adhesive" H 7312 5350 50  0001 C CNN
F 3 "~" H 7312 5350 50  0001 C CNN
F 4 "Fischer Elektronik" H -1500 -400 50  0001 C CNN "MFR"
F 5 "ICK S 40x40x20" H -1500 -400 50  0001 C CNN "MPN"
F 6 "RS" H -1500 -400 50  0001 C CNN "SPR"
F 7 "674-4835" H -1500 -400 50  0001 C CNN "SPN"
F 8 "-" H -1500 -400 50  0001 C CNN "SPURL"
	1    7300 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R48
U 1 1 5D680AE8
P 7800 2550
F 0 "R48" H 7870 2596 50  0000 L CNN
F 1 "0.1" H 7870 2505 50  0000 L CNN
F 2 "Resistor_SMD:R_2512_6332Metric" V 7730 2550 50  0001 C CNN
F 3 "~" H 7800 2550 50  0001 C CNN
F 4 "Bourns" H -1350 -1300 50  0001 C CNN "MFR"
F 5 "CRA2512-FZ-R100ELF" H -1350 -1300 50  0001 C CNN "MPN"
F 6 "RS" H -1350 -1300 50  0001 C CNN "SPR"
F 7 "693-4394P" H -1350 -1300 50  0001 C CNN "SPN"
F 8 "-" H -1350 -1300 50  0001 C CNN "SPURL"
	1    7800 2550
	1    0    0    -1  
$EndComp
$Comp
L power:-8V #PWR092
U 1 1 5D7466E5
P 1500 2250
F 0 "#PWR092" H 1500 2350 50  0001 C CNN
F 1 "-8V" H 1400 2400 50  0000 L CNN
F 2 "" H 1500 2250 50  0001 C CNN
F 3 "" H 1500 2250 50  0001 C CNN
	1    1500 2250
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR089
U 1 1 5D748090
P 1500 950
F 0 "#PWR089" H 1500 700 50  0001 C CNN
F 1 "GND" V 1505 822 50  0000 R CNN
F 2 "" H 1500 950 50  0001 C CNN
F 3 "" H 1500 950 50  0001 C CNN
	1    1500 950 
	1    0    0    1   
$EndComp
$Comp
L AOMPowerAmp-rescue:LT6015-Amplifier_Operational U?
U 1 1 5D899F16
P 4300 1700
AR Path="/5D899F16" Ref="U?"  Part="1" 
AR Path="/5D36080B/5D899F16" Ref="U26"  Part="1" 
F 0 "U26" H 4400 1950 50  0000 L CNN
F 1 "LT6015" H 4400 1850 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4200 1500 50  0001 L CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/601567ff.pdf" H 4300 1900 50  0001 C CNN
F 4 "Linear Technology" H 500 -4300 50  0001 C CNN "MFR"
F 5 "LT6015IS5#TRMPBF" H 500 -4300 50  0001 C CNN "MPN"
F 6 "RS" H 500 -4300 50  0001 C CNN "SPR"
F 7 "164-6077P" H 500 -4300 50  0001 C CNN "SPN"
F 8 "-" H 500 -4300 50  0001 C CNN "SPURL"
	1    4300 1700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR090
U 1 1 5D8CA00F
P 4200 1300
F 0 "#PWR090" H 4200 1050 50  0001 C CNN
F 1 "GND" H 4205 1127 50  0000 C CNN
F 2 "" H 4200 1300 50  0001 C CNN
F 3 "" H 4200 1300 50  0001 C CNN
	1    4200 1300
	-1   0    0    1   
$EndComp
$Comp
L Device:C C69
U 1 1 5D8E1043
P 4450 2450
F 0 "C69" H 4565 2496 50  0000 L CNN
F 1 "100n" H 4565 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4488 2300 50  0001 C CNN
F 3 "~" H 4450 2450 50  0001 C CNN
F 4 "Wurth Elektronik" H 500 -4300 50  0001 C CNN "MFR"
F 5 "885012206095" H 500 -4300 50  0001 C CNN "MPN"
F 6 "Distrelec" H 500 -4300 50  0001 C CNN "SPR"
F 7 "300-67-855" H 500 -4300 50  0001 C CNN "SPN"
F 8 "-" H 500 -4300 50  0001 C CNN "SPURL"
	1    4450 2450
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR094
U 1 1 5D8F939D
P 4700 2450
F 0 "#PWR094" H 4700 2200 50  0001 C CNN
F 1 "GND" H 4705 2277 50  0000 C CNN
F 2 "" H 4700 2450 50  0001 C CNN
F 3 "" H 4700 2450 50  0001 C CNN
	1    4700 2450
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR097
U 1 1 5D975723
P 2050 3200
F 0 "#PWR097" H 2050 2950 50  0001 C CNN
F 1 "GND" H 2055 3027 50  0000 C CNN
F 2 "" H 2050 3200 50  0001 C CNN
F 3 "" H 2050 3200 50  0001 C CNN
	1    2050 3200
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_POT_TRIM RV1
U 1 1 5D856252
P 1500 1600
F 0 "RV1" H 1431 1646 50  0000 R CNN
F 1 "3296Y-1-103LF" H 1431 1555 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296Y_Vertical" H 1500 1600 50  0001 C CNN
F 3 "~" H 1500 1600 50  0001 C CNN
F 4 "Bourns" H -700 -4300 50  0001 C CNN "MFR"
F 5 "3296Y-1-103LF" H -700 -4300 50  0001 C CNN "MPN"
F 6 "RS" H -700 -4300 50  0001 C CNN "SPR"
F 7 "167-3405" H -700 -4300 50  0001 C CNN "SPN"
F 8 "-" H -700 -4300 50  0001 C CNN "SPURL"
	1    1500 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R42
U 1 1 5D85596F
P 1500 1200
F 0 "R42" H 1570 1246 50  0000 L CNN
F 1 "10K" H 1570 1155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1430 1200 50  0001 C CNN
F 3 "~" H 1500 1200 50  0001 C CNN
F 4 "-" H -700 -4300 50  0001 C CNN "MFR"
F 5 "-" H -700 -4300 50  0001 C CNN "MPN"
F 6 "-" H -700 -4300 50  0001 C CNN "SPR"
F 7 "-" H -700 -4300 50  0001 C CNN "SPN"
F 8 "-" H -700 -4300 50  0001 C CNN "SPURL"
	1    1500 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R44
U 1 1 5D856A9D
P 1500 2000
F 0 "R44" H 1570 2046 50  0000 L CNN
F 1 "10K" H 1570 1955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1430 2000 50  0001 C CNN
F 3 "~" H 1500 2000 50  0001 C CNN
F 4 "-" H -700 -4300 50  0001 C CNN "MFR"
F 5 "-" H -700 -4300 50  0001 C CNN "MPN"
F 6 "-" H -700 -4300 50  0001 C CNN "SPR"
F 7 "-" H -700 -4300 50  0001 C CNN "SPN"
F 8 "-" H -700 -4300 50  0001 C CNN "SPURL"
	1    1500 2000
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP18
U 1 1 5DA22699
P 5400 1600
F 0 "TP18" V 5354 1788 50  0000 L CNN
F 1 "TestPoint" V 5445 1788 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5600 1600 50  0001 C CNN
F 3 "~" H 5600 1600 50  0001 C CNN
F 4 "-" H -1400 -2600 50  0001 C CNN "MFR"
F 5 "-" H -1400 -2600 50  0001 C CNN "MPN"
F 6 "-" H -1400 -2600 50  0001 C CNN "SPR"
F 7 "-" H -1400 -2600 50  0001 C CNN "SPN"
F 8 "-" H -1400 -2600 50  0001 C CNN "SPURL"
	1    5400 1600
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP19
U 1 1 5DA2E92F
P 7550 3750
F 0 "TP19" V 7745 3822 50  0000 C CNN
F 1 "TestPoint" V 7654 3822 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 7750 3750 50  0001 C CNN
F 3 "~" H 7750 3750 50  0001 C CNN
F 4 "-" H -300 800 50  0001 C CNN "MFR"
F 5 "-" H -300 800 50  0001 C CNN "MPN"
F 6 "-" H -300 800 50  0001 C CNN "SPR"
F 7 "-" H -300 800 50  0001 C CNN "SPN"
F 8 "-" H -300 800 50  0001 C CNN "SPURL"
	1    7550 3750
	0    -1   -1   0   
$EndComp
$Comp
L AOMPowerAmp-rescue:LT6015-Amplifier_Operational U?
U 1 1 5DC4FE9C
P 8950 2550
AR Path="/5DC4FE9C" Ref="U?"  Part="1" 
AR Path="/5D36080B/5DC4FE9C" Ref="U28"  Part="1" 
F 0 "U28" H 9050 2800 50  0000 L CNN
F 1 "LT6015" H 9050 2700 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 8850 2350 50  0001 L CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/601567ff.pdf" H 8950 2750 50  0001 C CNN
F 4 "Linear Technology" H -300 -650 50  0001 C CNN "MFR"
F 5 "LT6015IS5#TRMPBF" H -300 -650 50  0001 C CNN "MPN"
F 6 "RS" H -300 -650 50  0001 C CNN "SPR"
F 7 "164-6077P" H -300 -650 50  0001 C CNN "SPN"
F 8 "-" H -300 -650 50  0001 C CNN "SPURL"
	1    8950 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R46
U 1 1 5DC52362
P 8350 2300
F 0 "R46" V 8143 2300 50  0000 C CNN
F 1 "100" V 8234 2300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8280 2300 50  0001 C CNN
F 3 "~" H 8350 2300 50  0001 C CNN
F 4 "-" H -250 -650 50  0001 C CNN "MFR"
F 5 "-" H -250 -650 50  0001 C CNN "MPN"
F 6 "-" H -250 -650 50  0001 C CNN "SPR"
F 7 "-" H -250 -650 50  0001 C CNN "SPN"
F 8 "-" H -250 -650 50  0001 C CNN "SPURL"
	1    8350 2300
	0    1    1    0   
$EndComp
$Comp
L Device:R R52
U 1 1 5DC55513
P 8350 2800
F 0 "R52" V 8557 2800 50  0000 C CNN
F 1 "100" V 8466 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8280 2800 50  0001 C CNN
F 3 "~" H 8350 2800 50  0001 C CNN
F 4 "-" H -250 -650 50  0001 C CNN "MFR"
F 5 "-" H -250 -650 50  0001 C CNN "MPN"
F 6 "-" H -250 -650 50  0001 C CNN "SPR"
F 7 "-" H -250 -650 50  0001 C CNN "SPN"
F 8 "-" H -250 -650 50  0001 C CNN "SPURL"
	1    8350 2800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R49
U 1 1 5DCD6D7D
P 9500 2550
F 0 "R49" V 9293 2550 50  0000 C CNN
F 1 "100 1%" V 9384 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9430 2550 50  0001 C CNN
F 3 "~" H 9500 2550 50  0001 C CNN
F 4 "RND" H -300 -650 50  0001 C CNN "MFR"
F 5 "1550603SAF1000T5E" H -300 -650 50  0001 C CNN "MPN"
F 6 "Distrelec" H -300 -650 50  0001 C CNN "SPR"
F 7 "301-09-292" H -300 -650 50  0001 C CNN "SPN"
F 8 "-" H -300 -650 50  0001 C CNN "SPURL"
	1    9500 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R R56
U 1 1 5DD36ECE
P 10000 3350
F 0 "R56" H 10070 3396 50  0000 L CNN
F 1 "1K" H 10070 3305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9930 3350 50  0001 C CNN
F 3 "~" H 10000 3350 50  0001 C CNN
F 4 "RND" H -300 -500 50  0001 C CNN "MFR"
F 5 "1550603SAF1001T5E" H -300 -500 50  0001 C CNN "MPN"
F 6 "Distrelec" H -300 -500 50  0001 C CNN "SPR"
F 7 "301-09-293" H -300 -500 50  0001 C CNN "SPN"
F 8 "-" H -300 -500 50  0001 C CNN "SPURL"
	1    10000 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5DD37E1D
P 10000 4300
F 0 "#PWR0102" H 10000 4050 50  0001 C CNN
F 1 "GND" H 10005 4127 50  0000 C CNN
F 2 "" H 10000 4300 50  0001 C CNN
F 3 "" H 10000 4300 50  0001 C CNN
	1    10000 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR096
U 1 1 5DE222F8
P 8850 2950
F 0 "#PWR096" H 8850 2700 50  0001 C CNN
F 1 "GND" H 8855 2777 50  0000 C CNN
F 2 "" H 8850 2950 50  0001 C CNN
F 3 "" H 8850 2950 50  0001 C CNN
	1    8850 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR091
U 1 1 5DE64819
P 9350 1700
F 0 "#PWR091" H 9350 1450 50  0001 C CNN
F 1 "GND" H 9355 1527 50  0000 C CNN
F 2 "" H 9350 1700 50  0001 C CNN
F 3 "" H 9350 1700 50  0001 C CNN
	1    9350 1700
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C66
U 1 1 5DF63C28
P 9100 1700
F 0 "C66" V 8848 1700 50  0000 C CNN
F 1 "100n" V 8939 1700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9138 1550 50  0001 C CNN
F 3 "~" H 9100 1700 50  0001 C CNN
F 4 "Wurth Elektronik" H -300 -950 50  0001 C CNN "MFR"
F 5 "885012206095" H -300 -950 50  0001 C CNN "MPN"
F 6 "Distrelec" H -300 -950 50  0001 C CNN "SPR"
F 7 "300-67-855" H -300 -950 50  0001 C CNN "SPN"
F 8 "-" H -300 -950 50  0001 C CNN "SPURL"
	1    9100 1700
	0    1    1    0   
$EndComp
$Comp
L power:-8V #PWR095
U 1 1 5E0C4B10
P 4200 2700
F 0 "#PWR095" H 4200 2800 50  0001 C CNN
F 1 "-8V" V 4215 2828 50  0000 L CNN
F 2 "" H 4200 2700 50  0001 C CNN
F 3 "" H 4200 2700 50  0001 C CNN
	1    4200 2700
	1    0    0    1   
$EndComp
$Comp
L Motor:Fan M1
U 1 1 5E4922B6
P 1100 6950
F 0 "M1" H 1258 7046 50  0000 L CNN
F 1 "9GA0612L9001" H 1258 6955 50  0000 L CNN
F 2 "AOMPowerAmp:Fan_60x60mm_Horizontal" H 1100 6960 50  0001 C CNN
F 3 "~" H 1100 6960 50  0001 C CNN
F 4 "Sanyo Denki" H -9100 1650 50  0001 C CNN "MFR"
F 5 "9GA0612L9001" H -9100 1650 50  0001 C CNN "MPN"
F 6 "RS" H -9100 1650 50  0001 C CNN "SPR"
F 7 "885-5085" H -9100 1650 50  0001 C CNN "SPN"
F 8 "-" H -9100 1650 50  0001 C CNN "SPURL"
	1    1100 6950
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0106
U 1 1 5E493FF8
P 1100 6550
F 0 "#PWR0106" H 1100 6400 50  0001 C CNN
F 1 "+12V" H 1115 6723 50  0000 C CNN
F 2 "" H 1100 6550 50  0001 C CNN
F 3 "" H 1100 6550 50  0001 C CNN
	1    1100 6550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5E4AB3CF
P 1100 7250
F 0 "#PWR0109" H 1100 7000 50  0001 C CNN
F 1 "GND" H 1105 7077 50  0000 C CNN
F 2 "" H 1100 7250 50  0001 C CNN
F 3 "" H 1100 7250 50  0001 C CNN
	1    1100 7250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J8
U 1 1 5E57409A
P 2650 7000
F 0 "J8" H 2568 6675 50  0000 C CNN
F 1 "1715721" H 2568 6766 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-3-2-5.08_1x02_P5.08mm_Horizontal" H 2650 7000 50  0001 C CNN
F 3 "~" H 2650 7000 50  0001 C CNN
F 4 "Phoenix Contact" H -7500 750 50  0001 C CNN "MFR"
F 5 "1715721" H -7500 750 50  0001 C CNN "MPN"
F 6 "RS" H -7500 750 50  0001 C CNN "SPR"
F 7 "193-0564" H -7500 750 50  0001 C CNN "SPN"
F 8 "-" H -7500 750 50  0001 C CNN "SPURL"
	1    2650 7000
	-1   0    0    1   
$EndComp
$Comp
L power:+12V #PWR0107
U 1 1 5E575CE9
P 2950 6900
F 0 "#PWR0107" H 2950 6750 50  0001 C CNN
F 1 "+12V" V 2965 7028 50  0000 L CNN
F 2 "" H 2950 6900 50  0001 C CNN
F 3 "" H 2950 6900 50  0001 C CNN
	1    2950 6900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5E58119F
P 2950 7000
F 0 "#PWR0108" H 2950 6750 50  0001 C CNN
F 1 "GND" V 2955 6872 50  0000 R CNN
F 2 "" H 2950 7000 50  0001 C CNN
F 3 "" H 2950 7000 50  0001 C CNN
	1    2950 7000
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C67
U 1 1 5D5B175D
P 5400 2050
F 0 "C67" H 5515 2096 50  0000 L CNN
F 1 "100n" H 5515 2005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5438 1900 50  0001 C CNN
F 3 "~" H 5400 2050 50  0001 C CNN
F 4 "Wurth Elektronik" H 700 -4300 50  0001 C CNN "MFR"
F 5 "885012206095" H 700 -4300 50  0001 C CNN "MPN"
F 6 "Distrelec" H 700 -4300 50  0001 C CNN "SPR"
F 7 "300-67-855" H 700 -4300 50  0001 C CNN "SPN"
F 8 "-" H 700 -4300 50  0001 C CNN "SPURL"
	1    5400 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R47
U 1 1 5D5B1FE9
P 5400 2550
F 0 "R47" H 5470 2596 50  0000 L CNN
F 1 "49R9" H 5470 2505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5330 2550 50  0001 C CNN
F 3 "~" H 5400 2550 50  0001 C CNN
F 4 "Vishay" H 700 -4300 50  0001 C CNN "MFR"
F 5 "CRCW060349R9FKEA" H 700 -4300 50  0001 C CNN "MPN"
F 6 "RS" H 700 -4300 50  0001 C CNN "SPR"
F 7 "679-0459P" H 700 -4300 50  0001 C CNN "SPN"
F 8 "-" H 700 -4300 50  0001 C CNN "SPURL"
	1    5400 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR099
U 1 1 5D5B2AED
P 5400 3200
F 0 "#PWR099" H 5400 2950 50  0001 C CNN
F 1 "GND" H 5405 3027 50  0000 C CNN
F 2 "" H 5400 3200 50  0001 C CNN
F 3 "" H 5400 3200 50  0001 C CNN
	1    5400 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R57
U 1 1 5D71E04D
P 10000 3750
F 0 "R57" H 10070 3796 50  0000 L CNN
F 1 "1K" H 10070 3705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9930 3750 50  0001 C CNN
F 3 "~" H 10000 3750 50  0001 C CNN
F 4 "RND" H -300 -100 50  0001 C CNN "MFR"
F 5 "1550603SAF1001T5E" H -300 -100 50  0001 C CNN "MPN"
F 6 "Distrelec" H -300 -100 50  0001 C CNN "SPR"
F 7 "301-09-293" H -300 -100 50  0001 C CNN "SPN"
F 8 "-" H -300 -100 50  0001 C CNN "SPURL"
	1    10000 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C68
U 1 1 5D757C7A
P 5850 2050
F 0 "C68" H 5965 2096 50  0000 L CNN
F 1 "100n" H 5965 2005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5888 1900 50  0001 C CNN
F 3 "~" H 5850 2050 50  0001 C CNN
F 4 "Wurth Elektronik" H 700 -4300 50  0001 C CNN "MFR"
F 5 "885012206095" H 700 -4300 50  0001 C CNN "MPN"
F 6 "Distrelec" H 700 -4300 50  0001 C CNN "SPR"
F 7 "300-67-855" H 700 -4300 50  0001 C CNN "SPN"
F 8 "-" H 700 -4300 50  0001 C CNN "SPURL"
	1    5850 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R53
U 1 1 5D7BEF99
P 5400 2950
F 0 "R53" H 5470 2996 50  0000 L CNN
F 1 "100" H 5470 2905 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5330 2950 50  0001 C CNN
F 3 "~" H 5400 2950 50  0001 C CNN
F 4 "-" H 700 -4300 50  0001 C CNN "MFR"
F 5 "-" H 700 -4300 50  0001 C CNN "MPN"
F 6 "-" H 700 -4300 50  0001 C CNN "SPR"
F 7 "-" H 700 -4300 50  0001 C CNN "SPN"
F 8 "-" H 700 -4300 50  0001 C CNN "SPURL"
	1    5400 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R43
U 1 1 5D5BEA86
P 5650 1700
F 0 "R43" V 5443 1700 50  0000 C CNN
F 1 "4.02" V 5534 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5580 1700 50  0001 C CNN
F 3 "~" H 5650 1700 50  0001 C CNN
F 4 "Stackpole" H 700 -4300 50  0001 C CNN "MFR"
F 5 "RMCF0603FT4R02" H 700 -4300 50  0001 C CNN "MPN"
F 6 "Digikey" H 700 -4300 50  0001 C CNN "SPR"
F 7 "RMCF0603FT4R02TR-ND" H 700 -4300 50  0001 C CNN "SPN"
F 8 "-" H 700 -4300 50  0001 C CNN "SPURL"
	1    5650 1700
	0    1    1    0   
$EndComp
$Comp
L Device:R R45
U 1 1 5D99EC98
P 8050 2300
F 0 "R45" V 7843 2300 50  0000 C CNN
F 1 "100" V 7934 2300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7980 2300 50  0001 C CNN
F 3 "~" H 8050 2300 50  0001 C CNN
F 4 "-" H -550 -650 50  0001 C CNN "MFR"
F 5 "-" H -550 -650 50  0001 C CNN "MPN"
F 6 "-" H -550 -650 50  0001 C CNN "SPR"
F 7 "-" H -550 -650 50  0001 C CNN "SPN"
F 8 "-" H -550 -650 50  0001 C CNN "SPURL"
	1    8050 2300
	0    1    1    0   
$EndComp
$Comp
L Device:R R51
U 1 1 5D9C2A30
P 8050 2800
F 0 "R51" V 8257 2800 50  0000 C CNN
F 1 "100" V 8166 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7980 2800 50  0001 C CNN
F 3 "~" H 8050 2800 50  0001 C CNN
F 4 "-" H -550 -650 50  0001 C CNN "MFR"
F 5 "-" H -550 -650 50  0001 C CNN "MPN"
F 6 "-" H -550 -650 50  0001 C CNN "SPR"
F 7 "-" H -550 -650 50  0001 C CNN "SPN"
F 8 "-" H -550 -650 50  0001 C CNN "SPURL"
	1    8050 2800
	0    -1   -1   0   
$EndComp
$Comp
L Relay_SolidState:CPC1117N U27
U 1 1 5DB744C2
P 2850 2150
F 0 "U27" H 2850 1833 50  0000 C CNN
F 1 "CPC1117N" H 2850 1924 50  0000 C CNN
F 2 "Package_SO:SOP-4_3.8x4.1mm_P2.54mm" H 2650 1950 50  0001 L CIN
F 3 "http://www.ixysic.com/home/pdfs.nsf/www/CPC1117N.pdf/$file/CPC1117N.pdf" H 2800 2150 50  0001 L CNN
F 4 "-" H 300 -4300 50  0001 C CNN "MFR"
F 5 "-" H 300 -4300 50  0001 C CNN "MPN"
F 6 "-" H 300 -4300 50  0001 C CNN "SPR"
F 7 "-" H 300 -4300 50  0001 C CNN "SPN"
F 8 "-" H 300 -4300 50  0001 C CNN "SPURL"
	1    2850 2150
	1    0    0    1   
$EndComp
$Comp
L power:-8V #PWR093
U 1 1 5DC20AC4
P 3250 2350
F 0 "#PWR093" H 3250 2450 50  0001 C CNN
F 1 "-8V" H 3150 2500 50  0000 L CNN
F 2 "" H 3250 2350 50  0001 C CNN
F 3 "" H 3250 2350 50  0001 C CNN
	1    3250 2350
	1    0    0    1   
$EndComp
$Comp
L Device:R R55
U 1 1 5DC69EF2
P 2450 3050
F 0 "R55" H 2520 3096 50  0000 L CNN
F 1 "100" H 2520 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2380 3050 50  0001 C CNN
F 3 "~" H 2450 3050 50  0001 C CNN
F 4 "-" H 300 -3950 50  0001 C CNN "MFR"
F 5 "-" H 300 -3950 50  0001 C CNN "MPN"
F 6 "-" H 300 -3950 50  0001 C CNN "SPR"
F 7 "-" H 300 -3950 50  0001 C CNN "SPN"
F 8 "-" H 300 -3950 50  0001 C CNN "SPURL"
	1    2450 3050
	1    0    0    1   
$EndComp
$Comp
L Device:R R50
U 1 1 5DC6A832
P 2450 2750
F 0 "R50" H 2520 2796 50  0000 L CNN
F 1 "100" H 2520 2705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2380 2750 50  0001 C CNN
F 3 "~" H 2450 2750 50  0001 C CNN
F 4 "-" H 300 -4550 50  0001 C CNN "MFR"
F 5 "-" H 300 -4550 50  0001 C CNN "MPN"
F 6 "-" H 300 -4550 50  0001 C CNN "SPR"
F 7 "-" H 300 -4550 50  0001 C CNN "SPN"
F 8 "-" H 300 -4550 50  0001 C CNN "SPURL"
	1    2450 2750
	1    0    0    1   
$EndComp
Text HLabel 5400 5200 0    50   Input ~ 0
RF_In
Text HLabel 8800 5200 2    50   Output ~ 0
RF_Out
Text Notes 6150 6150 0    50   ~ 0
Biasing network taken from the\nreference design of the HMC1099PM5E
Text Notes 6850 4150 0    50   ~ 0
Power sequence order:\n1. Gate pinch off (-8V)\n2. Drain (24V-30V)\n3. Gate work (-3V)
Text HLabel 7800 1200 1    50   Input ~ 0
V_DRAIN
Text Notes 7900 1650 0    50   ~ 0
for calibration of\nquiescent current\nadjust RV1 for 100mA
Text HLabel 10100 2950 2    50   Output ~ 0
CURRENT_SENSE
Text Notes 9750 2950 0    50   ~ 0
1V/A
Text Label 7800 3650 0    50   ~ 0
V_DD
Text Notes 1200 6650 0    50   ~ 0
60x60x10mm Fan\n50x50mm mounting
Text Notes 1600 1500 0    50   ~ 0
Poti: 10K
Text Notes 800  6150 0    50   ~ 0
add MOSFET switching fan
Text Notes 2800 3350 0    50   ~ 0
min. 1mA required\ncurrent limited by pull-up\nresistors as control output\nis open-drain\nVoltage drop over LED ~~1.2V
Wire Wire Line
	6400 5000 6400 5200
Wire Wire Line
	6400 5200 6300 5200
Wire Wire Line
	6400 5200 6750 5200
Wire Wire Line
	7350 5200 7800 5200
Wire Wire Line
	7800 5200 7800 5000
Wire Wire Line
	7800 5200 7900 5200
Wire Wire Line
	8300 5300 8300 5200
Wire Wire Line
	8300 5200 8200 5200
Wire Wire Line
	6000 5200 5900 5200
Wire Wire Line
	5900 5200 5900 5300
Wire Wire Line
	5800 5200 5900 5200
Wire Wire Line
	4950 3850 4950 3750
Wire Wire Line
	4950 3750 5400 3750
Wire Wire Line
	5400 3750 5400 3850
Wire Wire Line
	5400 3750 5900 3750
Wire Wire Line
	5900 3750 5900 3850
Wire Wire Line
	5900 3750 6400 3750
Wire Wire Line
	4950 4250 4950 4150
Wire Wire Line
	4950 4250 5400 4250
Wire Wire Line
	5400 4250 5400 4150
Wire Wire Line
	5400 4250 5900 4250
Wire Wire Line
	5900 4250 5900 4150
Wire Wire Line
	9250 3850 9250 3750
Wire Wire Line
	9250 3750 8800 3750
Wire Wire Line
	8800 3750 8800 3850
Wire Wire Line
	8800 3750 8300 3750
Wire Wire Line
	8300 3750 8300 3850
Wire Wire Line
	8300 3750 7800 3750
Wire Wire Line
	9250 4250 9250 4150
Wire Wire Line
	9250 4250 8800 4250
Wire Wire Line
	8800 4250 8800 4150
Wire Wire Line
	8800 4250 8300 4250
Wire Wire Line
	8300 4250 8300 4150
Wire Wire Line
	6950 5700 6950 5600
Wire Wire Line
	6950 5600 7050 5600
Wire Wire Line
	7050 5600 7050 5500
Wire Wire Line
	6950 5600 6950 5500
Wire Wire Line
	8300 5700 8300 5600
Wire Wire Line
	5900 5700 5900 5600
Wire Wire Line
	8300 5200 8400 5200
Wire Wire Line
	8700 5200 8800 5200
Wire Wire Line
	5400 5200 5500 5200
Wire Wire Line
	7300 5600 7300 5500
Wire Wire Line
	1500 950  1500 1050
Wire Wire Line
	1500 1350 1500 1450
Wire Wire Line
	1500 1750 1500 1850
Wire Wire Line
	3900 2150 3900 1800
Wire Wire Line
	3900 1800 4000 1800
Wire Wire Line
	4750 2150 4750 1700
Wire Wire Line
	4750 1700 4600 1700
Wire Wire Line
	3900 2150 4750 2150
Wire Wire Line
	4200 1300 4200 1400
Wire Wire Line
	4200 2700 4200 2450
Wire Wire Line
	5400 1600 5400 1700
Wire Wire Line
	7550 3750 7800 3750
Wire Wire Line
	8500 2300 8550 2300
Wire Wire Line
	8550 2300 8550 2450
Wire Wire Line
	8650 2650 8550 2650
Wire Wire Line
	8550 2650 8550 2800
Wire Wire Line
	8550 2800 8500 2800
Wire Wire Line
	9350 2550 9250 2550
Wire Wire Line
	10000 2350 10000 2150
Wire Wire Line
	8550 2450 8650 2450
Wire Wire Line
	10000 2150 8550 2150
Wire Wire Line
	8550 2150 8550 2300
Wire Wire Line
	8850 2950 8850 2850
Wire Wire Line
	10100 2950 10000 2950
Wire Wire Line
	9350 1700 9250 1700
Wire Wire Line
	4300 2450 4200 2450
Wire Wire Line
	4200 2450 4200 2000
Wire Wire Line
	4700 2450 4600 2450
Wire Wire Line
	1100 6550 1100 6650
Wire Wire Line
	1100 7250 1100 7150
Wire Wire Line
	7300 5600 7050 5600
Wire Wire Line
	2950 6900 2850 6900
Wire Wire Line
	2950 7000 2850 7000
Wire Wire Line
	9700 2550 9650 2550
Wire Wire Line
	5400 3200 5400 3100
Wire Wire Line
	5400 2400 5400 2300
Wire Wire Line
	5400 1900 5400 1800
Wire Wire Line
	10000 3600 10000 3500
Wire Wire Line
	5850 1900 5850 1800
Wire Wire Line
	5850 1800 5400 1800
Wire Wire Line
	5400 1800 5400 1700
Wire Wire Line
	5400 2300 5850 2300
Wire Wire Line
	5850 2300 5850 2200
Wire Wire Line
	5400 2300 5400 2200
Wire Wire Line
	5400 2700 5400 2800
Wire Wire Line
	5500 1700 5400 1700
Wire Wire Line
	7900 2300 7800 2300
Wire Wire Line
	7800 2300 7800 2400
Wire Wire Line
	7800 2700 7800 2800
Wire Wire Line
	7900 2800 7800 2800
Wire Wire Line
	3150 2050 3250 2050
Wire Wire Line
	3250 2050 3250 1600
Wire Wire Line
	3250 2350 3250 2250
Wire Wire Line
	3250 2250 3150 2250
Connection ~ 6400 5200
Connection ~ 7800 5200
Connection ~ 5900 5200
Connection ~ 5400 3750
Connection ~ 5900 3750
Connection ~ 5400 4250
Connection ~ 8800 3750
Connection ~ 8300 3750
Connection ~ 8800 4250
Connection ~ 7800 3750
Connection ~ 6950 5600
Connection ~ 8300 5200
Connection ~ 8550 2300
Connection ~ 4200 2450
Connection ~ 7050 5600
Connection ~ 5400 1800
Connection ~ 5400 2300
Connection ~ 5400 1700
$Comp
L power:+5V #PWR098
U 1 1 5F2088F5
P 2450 3200
F 0 "#PWR098" H 2450 3050 50  0001 C CNN
F 1 "+5V" H 2450 3350 50  0000 C CNN
F 2 "" H 2450 3200 50  0001 C CNN
F 3 "" H 2450 3200 50  0001 C CNN
	1    2450 3200
	-1   0    0    1   
$EndComp
Wire Wire Line
	2450 2250 2550 2250
$Comp
L Transistor_BJT:BC817 Q2
U 1 1 5F23C1EF
P 1950 3000
F 0 "Q2" H 2141 3046 50  0000 L CNN
F 1 "BC817" H 2141 2955 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2150 2925 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC817.pdf" H 1950 3000 50  0001 L CNN
	1    1950 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 2050 2050 2050
Wire Wire Line
	1500 2250 1500 2150
$Comp
L Device:R R54
U 1 1 5F27919C
P 1500 3000
F 0 "R54" V 1300 2950 50  0000 L CNN
F 1 "2k2" V 1400 2950 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1430 3000 50  0001 C CNN
F 3 "~" H 1500 3000 50  0001 C CNN
F 4 "-" H -700 -3300 50  0001 C CNN "MFR"
F 5 "-" H -700 -3300 50  0001 C CNN "MPN"
F 6 "-" H -700 -3300 50  0001 C CNN "SPR"
F 7 "-" H -700 -3300 50  0001 C CNN "SPN"
F 8 "-" H -700 -3300 50  0001 C CNN "SPURL"
	1    1500 3000
	0    1    1    0   
$EndComp
Text HLabel 1250 3000 0    50   Input ~ 0
WORK_ENABLE
Wire Wire Line
	9250 4300 9250 4250
Connection ~ 9250 4250
Wire Wire Line
	5800 1700 6400 1700
Text Notes 6100 1650 0    50   ~ 0
V_GATE
Wire Wire Line
	1650 1600 3250 1600
Connection ~ 3250 1600
Wire Wire Line
	3250 1600 4000 1600
Wire Wire Line
	1250 3000 1350 3000
Wire Wire Line
	1750 3000 1650 3000
Wire Wire Line
	4750 1700 5400 1700
Connection ~ 4750 1700
$Comp
L power:GND #PWR0100
U 1 1 5F443F80
P 4950 4300
F 0 "#PWR0100" H 4950 4050 50  0001 C CNN
F 1 "GND" H 5050 4150 50  0000 R CNN
F 2 "" H 4950 4300 50  0001 C CNN
F 3 "" H 4950 4300 50  0001 C CNN
	1    4950 4300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4950 4300 4950 4250
Connection ~ 4950 4250
Wire Wire Line
	6400 3750 6400 4700
Wire Wire Line
	6400 1700 6400 3750
Connection ~ 6400 3750
Wire Wire Line
	10000 2750 10000 2950
$Comp
L Transistor_FET:BSP89 Q1
U 1 1 5DCF813F
P 9900 2550
F 0 "Q1" H 10106 2596 50  0000 L CNN
F 1 "BSP89" H 10106 2505 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 10100 2475 50  0001 L CIN
F 3 "https://www.infineon.com/dgdl/Infineon-BSP89-DS-v02_02-en.pdf?fileId=db3a30433b47825b013b4b8a07f90d55" H 9900 2550 50  0001 L CNN
F 4 "Infineon" H -300 -650 50  0001 C CNN "MFR"
F 5 "BSP89H6327XTSA1" H -300 -650 50  0001 C CNN "MPN"
F 6 "RS" H -300 -650 50  0001 C CNN "SPR"
F 7 "445-2281P" H -300 -650 50  0001 C CNN "SPN"
F 8 "-" H -300 -650 50  0001 C CNN "SPURL"
	1    9900 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 3900 10000 4300
Wire Wire Line
	10000 3200 10000 2950
Connection ~ 10000 2950
Wire Wire Line
	7800 1700 8850 1700
Wire Wire Line
	8850 2250 8850 1700
Connection ~ 8850 1700
Wire Wire Line
	8850 1700 8950 1700
Connection ~ 7800 2300
Wire Wire Line
	7800 1700 7800 2300
Wire Wire Line
	7800 2800 7800 3750
Connection ~ 7800 2800
Wire Wire Line
	7800 4700 7800 3750
Text HLabel 4750 1250 1    50   Input ~ 0
V_GATE_SENSE
Wire Wire Line
	7800 1200 7800 1700
Connection ~ 7800 1700
Wire Wire Line
	4750 1250 4750 1700
Wire Wire Line
	2050 2050 2050 2800
Wire Wire Line
	2450 2250 2450 2600
Wire Notes Line
	5450 4600 8700 4600
Wire Notes Line
	8700 4600 8700 6250
Wire Notes Line
	8700 6250 5450 6250
Wire Notes Line
	5450 6250 5450 4600
Text Notes 6750 4550 0    50   ~ 10
RF Power amplifier
$EndSCHEMATC
