EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 "Hendrik v. Raven"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR044
U 1 1 5D45FF82
P 4300 1800
F 0 "#PWR044" H 4300 1550 50  0001 C CNN
F 1 "GND" H 4305 1627 50  0000 C CNN
F 2 "" H 4300 1800 50  0001 C CNN
F 3 "" H 4300 1800 50  0001 C CNN
	1    4300 1800
	1    0    0    -1  
$EndComp
$Comp
L AOMPowerAmp-rescue:TCP-2-10X-RF U9
U 1 1 5D4132FB
P 2200 2700
F 0 "U9" H 2200 3125 50  0000 C CNN
F 1 "TCP-2-10X" H 2200 3034 50  0000 C CNN
F 2 "RF_Mini-Circuits:Mini-Circuits_DB1627" H 2050 2250 50  0001 C CNN
F 3 "https://ww2.minicircuits.com/pdfs/TCP-2-10X+.pdf" H 2150 2350 50  0001 C CNN
F 4 "-" H 0   0   50  0001 C CNN "MFR"
F 5 "-" H 0   0   50  0001 C CNN "MPN"
F 6 "-" H 0   0   50  0001 C CNN "SPR"
F 7 "-" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    2200 2700
	1    0    0    -1  
$EndComp
$Comp
L AOMPowerAmp-rescue:ADL5904-RF U10
U 1 1 5D413F2F
P 4550 5300
F 0 "U10" H 4250 5750 50  0000 C CNN
F 1 "ADL5904" H 4800 5750 50  0000 C CNN
F 2 "Package_CSP:LFCSP-16-1EP_3x3mm_P0.5mm_EP1.6x1.6mm_ThermalVias" H 4550 5300 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADL5904.pdf" H 4550 5350 50  0001 C CNN
F 4 "Analog" H 0   0   50  0001 C CNN "MFR"
F 5 "ADL5904ACPZN-R7" H 0   0   50  0001 C CNN "MPN"
F 6 "Mouser" H 0   0   50  0001 C CNN "SPR"
F 7 "584-ADL5904ACPZN-R7 " H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    4550 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R13
U 1 1 5D4202B5
P 2750 2700
F 0 "R13" H 2820 2746 50  0000 L CNN
F 1 "100" H 2820 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 2680 2700 50  0001 C CNN
F 3 "~" H 2750 2700 50  0001 C CNN
F 4 "Vishay" H 0   0   50  0001 C CNN "MFR"
F 5 "CRCW1206100RFKEA" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "160-54-708" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    2750 2700
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C17
U 1 1 5D420DD2
P 1700 2950
F 0 "C17" H 1815 2996 50  0000 L CNN
F 1 "1p5" H 1815 2905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1738 2800 50  0001 C CNN
F 3 "~" H 1700 2950 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012006046" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "300-67-421" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    1700 2950
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C16
U 1 1 5D4215CF
P 1250 2950
F 0 "C16" H 1365 2996 50  0000 L CNN
F 1 "1p5" H 1365 2905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1288 2800 50  0001 C CNN
F 3 "~" H 1250 2950 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012006046" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "300-67-421" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    1250 2950
	-1   0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR051
U 1 1 5D441D5E
P 4550 4050
F 0 "#PWR051" H 4550 3900 50  0001 C CNN
F 1 "+3V3" H 4565 4223 50  0000 C CNN
F 2 "" H 4550 4050 50  0001 C CNN
F 3 "" H 4550 4050 50  0001 C CNN
	1    4550 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C20
U 1 1 5D44275E
P 4800 4200
F 0 "C20" V 4548 4200 50  0000 C CNN
F 1 "100n" V 4639 4200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4838 4050 50  0001 C CNN
F 3 "~" H 4800 4200 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "300-67-855" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    4800 4200
	0    1    1    0   
$EndComp
$Comp
L Device:C C21
U 1 1 5D443041
P 4800 4600
F 0 "C21" V 4548 4600 50  0000 C CNN
F 1 "100p" V 4639 4600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4838 4450 50  0001 C CNN
F 3 "~" H 4800 4600 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012006057" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "300-67-432" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    4800 4600
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR054
U 1 1 5D4476FC
P 5050 4600
F 0 "#PWR054" H 5050 4350 50  0001 C CNN
F 1 "GND" V 5055 4472 50  0000 R CNN
F 2 "" H 5050 4600 50  0001 C CNN
F 3 "" H 5050 4600 50  0001 C CNN
	1    5050 4600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR053
U 1 1 5D447B82
P 5050 4200
F 0 "#PWR053" H 5050 3950 50  0001 C CNN
F 1 "GND" V 5055 4072 50  0000 R CNN
F 2 "" H 5050 4200 50  0001 C CNN
F 3 "" H 5050 4200 50  0001 C CNN
	1    5050 4200
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C24
U 1 1 5D44A495
P 3650 5950
F 0 "C24" H 3765 5996 50  0000 L CNN
F 1 "100n" H 3765 5905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3688 5800 50  0001 C CNN
F 3 "~" H 3650 5950 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "300-67-855" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    3650 5950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R16
U 1 1 5D44F778
P 4000 6450
F 0 "R16" H 4070 6496 50  0000 L CNN
F 1 "4.02" H 4070 6405 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3930 6450 50  0001 C CNN
F 3 "~" H 4000 6450 50  0001 C CNN
F 4 "Stackpole" H 0   0   50  0001 C CNN "MFR"
F 5 "RMCF0603FT4R02" H 0   0   50  0001 C CNN "MPN"
F 6 "Digikey" H 0   0   50  0001 C CNN "SPR"
F 7 "RMCF0603FT4R02TR-ND" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    4000 6450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C25
U 1 1 5D4501F3
P 4000 6850
F 0 "C25" H 4115 6896 50  0000 L CNN
F 1 "100n" H 4115 6805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4038 6700 50  0001 C CNN
F 3 "~" H 4000 6850 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "300-67-855" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    4000 6850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR059
U 1 1 5D450B5C
P 4000 7100
F 0 "#PWR059" H 4000 6850 50  0001 C CNN
F 1 "GND" H 4005 6927 50  0000 C CNN
F 2 "" H 4000 7100 50  0001 C CNN
F 3 "" H 4000 7100 50  0001 C CNN
	1    4000 7100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C22
U 1 1 5D451AF8
P 3200 5400
F 0 "C22" V 2948 5400 50  0000 C CNN
F 1 "470n" V 3039 5400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3238 5250 50  0001 C CNN
F 3 "~" H 3200 5400 50  0001 C CNN
F 4 "Wurth Elektronik" H -300 0   50  0001 C CNN "MFR"
F 5 "885012206050" H -300 0   50  0001 C CNN "MPN"
F 6 "Distrelec" H -300 0   50  0001 C CNN "SPR"
F 7 "300-67-810" H -300 0   50  0001 C CNN "SPN"
F 8 "-" H -300 0   50  0001 C CNN "SPURL"
	1    3200 5400
	0    1    1    0   
$EndComp
$Comp
L Device:R R15
U 1 1 5D452BF3
P 2900 5650
F 0 "R15" H 2970 5696 50  0000 L CNN
F 1 "82.5" H 2970 5605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2830 5650 50  0001 C CNN
F 3 "~" H 2900 5650 50  0001 C CNN
F 4 "RND" H 0   0   50  0001 C CNN "MFR"
F 5 "1550603SAF820JT5E" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "301-09-342" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    2900 5650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR056
U 1 1 5D454906
P 2900 5900
F 0 "#PWR056" H 2900 5650 50  0001 C CNN
F 1 "GND" H 2905 5727 50  0000 C CNN
F 2 "" H 2900 5900 50  0001 C CNN
F 3 "" H 2900 5900 50  0001 C CNN
	1    2900 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C23
U 1 1 5D479CA6
P 5050 5650
F 0 "C23" H 5165 5696 50  0000 L CNN
F 1 "100n" H 5165 5605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5088 5500 50  0001 C CNN
F 3 "~" H 5050 5650 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "300-67-855" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    5050 5650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR058
U 1 1 5D47AE99
P 5050 5900
F 0 "#PWR058" H 5050 5650 50  0001 C CNN
F 1 "GND" H 5055 5727 50  0000 C CNN
F 2 "" H 5050 5900 50  0001 C CNN
F 3 "" H 5050 5900 50  0001 C CNN
	1    5050 5900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR057
U 1 1 5D47BAC2
P 4550 5900
F 0 "#PWR057" H 4550 5650 50  0001 C CNN
F 1 "GND" H 4555 5727 50  0000 C CNN
F 2 "" H 4550 5900 50  0001 C CNN
F 3 "" H 4550 5900 50  0001 C CNN
	1    4550 5900
	1    0    0    -1  
$EndComp
$Comp
L AOMPowerAmp-rescue:ADL5904-RF U8
U 1 1 5D488258
P 8900 2300
F 0 "U8" H 8600 2750 50  0000 C CNN
F 1 "ADL5904" H 9150 2750 50  0000 C CNN
F 2 "Package_CSP:LFCSP-16-1EP_3x3mm_P0.5mm_EP1.6x1.6mm_ThermalVias" H 8900 2300 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADL5904.pdf" H 8900 2350 50  0001 C CNN
F 4 "Analog" H 0   0   50  0001 C CNN "MFR"
F 5 "ADL5904ACPZN-R7" H 0   0   50  0001 C CNN "MPN"
F 6 "Mouser" H 0   0   50  0001 C CNN "SPR"
F 7 "584-ADL5904ACPZN-R7 " H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    8900 2300
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR041
U 1 1 5D488262
P 8900 1050
F 0 "#PWR041" H 8900 900 50  0001 C CNN
F 1 "+3V3" H 8915 1223 50  0000 C CNN
F 2 "" H 8900 1050 50  0001 C CNN
F 3 "" H 8900 1050 50  0001 C CNN
	1    8900 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C12
U 1 1 5D48826C
P 9150 1200
F 0 "C12" V 8898 1200 50  0000 C CNN
F 1 "100n" V 8989 1200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9188 1050 50  0001 C CNN
F 3 "~" H 9150 1200 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "300-67-855" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    9150 1200
	0    1    1    0   
$EndComp
$Comp
L Device:C C13
U 1 1 5D488276
P 9150 1600
F 0 "C13" V 8898 1600 50  0000 C CNN
F 1 "100p" V 8989 1600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9188 1450 50  0001 C CNN
F 3 "~" H 9150 1600 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012006057" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "300-67-432" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    9150 1600
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR043
U 1 1 5D488287
P 9400 1600
F 0 "#PWR043" H 9400 1350 50  0001 C CNN
F 1 "GND" V 9405 1472 50  0000 R CNN
F 2 "" H 9400 1600 50  0001 C CNN
F 3 "" H 9400 1600 50  0001 C CNN
	1    9400 1600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR042
U 1 1 5D488291
P 9400 1200
F 0 "#PWR042" H 9400 950 50  0001 C CNN
F 1 "GND" V 9405 1072 50  0000 R CNN
F 2 "" H 9400 1200 50  0001 C CNN
F 3 "" H 9400 1200 50  0001 C CNN
	1    9400 1200
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C18
U 1 1 5D48829E
P 8000 2950
F 0 "C18" H 8115 2996 50  0000 L CNN
F 1 "100n" H 8115 2905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8038 2800 50  0001 C CNN
F 3 "~" H 8000 2950 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "300-67-855" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    8000 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 5D4882AE
P 8350 3450
F 0 "R14" H 8420 3496 50  0000 L CNN
F 1 "4.02" H 8420 3405 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8280 3450 50  0001 C CNN
F 3 "~" H 8350 3450 50  0001 C CNN
F 4 "Stackpole" H 0   0   50  0001 C CNN "MFR"
F 5 "RMCF0603FT4R02" H 0   0   50  0001 C CNN "MPN"
F 6 "Digikey" H 0   0   50  0001 C CNN "SPR"
F 7 "RMCF0603FT4R02TR-ND" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    8350 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C19
U 1 1 5D4882BA
P 8350 3850
F 0 "C19" H 8465 3896 50  0000 L CNN
F 1 "100n" H 8465 3805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8388 3700 50  0001 C CNN
F 3 "~" H 8350 3850 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "300-67-855" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    8350 3850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR052
U 1 1 5D4882C5
P 8350 4100
F 0 "#PWR052" H 8350 3850 50  0001 C CNN
F 1 "GND" H 8355 3927 50  0000 C CNN
F 2 "" H 8350 4100 50  0001 C CNN
F 3 "" H 8350 4100 50  0001 C CNN
	1    8350 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C14
U 1 1 5D4882D0
P 7500 2400
F 0 "C14" V 7248 2400 50  0000 C CNN
F 1 "470n" V 7339 2400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7538 2250 50  0001 C CNN
F 3 "~" H 7500 2400 50  0001 C CNN
F 4 "Wurth Elektronik" H -350 0   50  0001 C CNN "MFR"
F 5 "885012206050" H -350 0   50  0001 C CNN "MPN"
F 6 "Distrelec" H -350 0   50  0001 C CNN "SPR"
F 7 "300-67-810" H -350 0   50  0001 C CNN "SPN"
F 8 "-" H -350 0   50  0001 C CNN "SPURL"
	1    7500 2400
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5D4882DB
P 7250 2650
F 0 "R12" H 7320 2696 50  0000 L CNN
F 1 "82.5" H 7320 2605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7180 2650 50  0001 C CNN
F 3 "~" H 7250 2650 50  0001 C CNN
F 4 "RND" H 0   0   50  0001 C CNN "MFR"
F 5 "1550603SAF820JT5E" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "301-09-342" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    7250 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR046
U 1 1 5D4882E7
P 7250 2900
F 0 "#PWR046" H 7250 2650 50  0001 C CNN
F 1 "GND" H 7255 2727 50  0000 C CNN
F 2 "" H 7250 2900 50  0001 C CNN
F 3 "" H 7250 2900 50  0001 C CNN
	1    7250 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C15
U 1 1 5D4882F4
P 9400 2650
F 0 "C15" H 9515 2696 50  0000 L CNN
F 1 "100n" H 9515 2605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9438 2500 50  0001 C CNN
F 3 "~" H 9400 2650 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012206095" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "300-67-855" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    9400 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR048
U 1 1 5D488300
P 9400 2900
F 0 "#PWR048" H 9400 2650 50  0001 C CNN
F 1 "GND" H 9405 2727 50  0000 C CNN
F 2 "" H 9400 2900 50  0001 C CNN
F 3 "" H 9400 2900 50  0001 C CNN
	1    9400 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR047
U 1 1 5D48830B
P 8900 2900
F 0 "#PWR047" H 8900 2650 50  0001 C CNN
F 1 "GND" H 8905 2727 50  0000 C CNN
F 2 "" H 8900 2900 50  0001 C CNN
F 3 "" H 8900 2900 50  0001 C CNN
	1    8900 2900
	1    0    0    -1  
$EndComp
$Comp
L AOMPowerAmp-rescue:PAT-7-RF AT4
U 1 1 5D4ADAA8
P 3600 2500
F 0 "AT4" H 3600 2917 50  0000 C CNN
F 1 "PAT-7" H 3600 2826 50  0000 C CNN
F 2 "RF_Mini-Circuits:Mini-Circuits_AF320_LandPatternPL-208" H 3600 2050 50  0001 C CNN
F 3 "https://ww2.minicircuits.com/pdfs/PAT-7+.pdf" H 3600 2525 50  0001 C CNN
F 4 "-" H 0   0   50  0001 C CNN "MFR"
F 5 "-" H 0   0   50  0001 C CNN "MPN"
F 6 "-" H 0   0   50  0001 C CNN "SPR"
F 7 "-" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    3600 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR049
U 1 1 5D4B9693
P 3400 3000
F 0 "#PWR049" H 3400 2750 50  0001 C CNN
F 1 "GND" H 3405 2827 50  0000 C CNN
F 2 "" H 3400 3000 50  0001 C CNN
F 3 "" H 3400 3000 50  0001 C CNN
	1    3400 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR055
U 1 1 5D4EFA3F
P 2200 5900
F 0 "#PWR055" H 2200 5650 50  0001 C CNN
F 1 "GND" H 2205 5727 50  0000 C CNN
F 2 "" H 2200 5900 50  0001 C CNN
F 3 "" H 2200 5900 50  0001 C CNN
	1    2200 5900
	1    0    0    -1  
$EndComp
$Comp
L AOMPowerAmp-rescue:PAT-7-RF AT5
U 1 1 5D4F32BE
P 2400 5400
F 0 "AT5" H 2400 5817 50  0000 C CNN
F 1 "PAT-7" H 2400 5726 50  0000 C CNN
F 2 "RF_Mini-Circuits:Mini-Circuits_AF320_LandPatternPL-208" H 2400 4950 50  0001 C CNN
F 3 "https://ww2.minicircuits.com/pdfs/PAT-7+.pdf" H 2400 5425 50  0001 C CNN
F 4 "-" H 0   0   50  0001 C CNN "MFR"
F 5 "-" H 0   0   50  0001 C CNN "MPN"
F 6 "-" H 0   0   50  0001 C CNN "SPR"
F 7 "-" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    2400 5400
	1    0    0    -1  
$EndComp
$Comp
L AOMPowerAmp-rescue:PAT-10-RF AT3
U 1 1 5D4F9669
P 6200 1500
F 0 "AT3" H 6200 1917 50  0000 C CNN
F 1 "PAT-10" H 6200 1826 50  0000 C CNN
F 2 "RF_Mini-Circuits:Mini-Circuits_AF320_LandPatternPL-208" H 6200 1050 50  0001 C CNN
F 3 "https://ww2.minicircuits.com/pdfs/PAT-10+.pdf" H 6200 1525 50  0001 C CNN
F 4 "-" H 0   0   50  0001 C CNN "MFR"
F 5 "-" H 0   0   50  0001 C CNN "MPN"
F 6 "-" H 0   0   50  0001 C CNN "SPR"
F 7 "-" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    6200 1500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR045
U 1 1 5D4FDDD2
P 6000 2000
F 0 "#PWR045" H 6000 1750 50  0001 C CNN
F 1 "GND" H 6005 1827 50  0000 C CNN
F 2 "" H 6000 2000 50  0001 C CNN
F 3 "" H 6000 2000 50  0001 C CNN
	1    6000 2000
	1    0    0    -1  
$EndComp
$Comp
L AOMPowerAmp-rescue:SYDC-20-13HP-RF U7
U 1 1 5D45CE06
P 4300 1400
F 0 "U7" H 4300 1767 50  0000 C CNN
F 1 "SYDC-20-13HP" H 4300 1676 50  0000 C CNN
F 2 "RF_Mini-Circuits:Mini-Circuits_AH202-1_LandPatternPL-246_ClockwisePinNumbering" H 4300 1400 50  0001 C CNN
F 3 "https://ww2.minicircuits.com/pdfs/SYDC-20-13HP+.pdf" H 4500 1050 50  0001 C CNN
F 4 "-" H 0   0   50  0001 C CNN "MFR"
F 5 "-" H 0   0   50  0001 C CNN "MPN"
F 6 "-" H 0   0   50  0001 C CNN "SPR"
F 7 "-" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    4300 1400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR050
U 1 1 5D824D2A
P 1700 3350
F 0 "#PWR050" H 1700 3100 50  0001 C CNN
F 1 "GND" H 1705 3177 50  0000 C CNN
F 2 "" H 1700 3350 50  0001 C CNN
F 3 "" H 1700 3350 50  0001 C CNN
	1    1700 3350
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP2
U 1 1 5DB60DA8
P 9800 2200
F 0 "TP2" H 9925 2275 50  0000 C CNN
F 1 "TestPoint_Flag" H 10060 2203 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 10000 2200 50  0001 C CNN
F 3 "~" H 10000 2200 50  0001 C CNN
F 4 "-" H 0   0   50  0001 C CNN "MFR"
F 5 "-" H 0   0   50  0001 C CNN "MPN"
F 6 "-" H 0   0   50  0001 C CNN "SPR"
F 7 "-" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    9800 2200
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP6
U 1 1 5DB667D8
P 5500 5400
F 0 "TP6" H 5625 5475 50  0000 C CNN
F 1 "TestPoint_Flag" H 5760 5403 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5700 5400 50  0001 C CNN
F 3 "~" H 5700 5400 50  0001 C CNN
F 4 "-" H 0   0   50  0001 C CNN "MFR"
F 5 "-" H 0   0   50  0001 C CNN "MPN"
F 6 "-" H 0   0   50  0001 C CNN "SPR"
F 7 "-" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    5500 5400
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP5
U 1 1 5DB66F79
P 5500 5200
F 0 "TP5" H 5625 5275 50  0000 C CNN
F 1 "TestPoint_Flag" H 5760 5203 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5700 5200 50  0001 C CNN
F 3 "~" H 5700 5200 50  0001 C CNN
F 4 "-" H 0   0   50  0001 C CNN "MFR"
F 5 "-" H 0   0   50  0001 C CNN "MPN"
F 6 "-" H 0   0   50  0001 C CNN "SPR"
F 7 "-" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    5500 5200
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP4
U 1 1 5DB6C759
P 3850 5200
F 0 "TP4" H 3975 5275 50  0000 C CNN
F 1 "TestPoint_Flag" H 4110 5203 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 4050 5200 50  0001 C CNN
F 3 "~" H 4050 5200 50  0001 C CNN
F 4 "-" H 0   0   50  0001 C CNN "MFR"
F 5 "-" H 0   0   50  0001 C CNN "MPN"
F 6 "-" H 0   0   50  0001 C CNN "SPR"
F 7 "-" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    3850 5200
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP3
U 1 1 5DB750BF
P 9800 2400
F 0 "TP3" H 9925 2475 50  0000 C CNN
F 1 "TestPoint_Flag" H 10060 2403 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 10000 2400 50  0001 C CNN
F 3 "~" H 10000 2400 50  0001 C CNN
F 4 "-" H 0   0   50  0001 C CNN "MFR"
F 5 "-" H 0   0   50  0001 C CNN "MPN"
F 6 "-" H 0   0   50  0001 C CNN "SPR"
F 7 "-" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    9800 2400
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint_Flag TP1
U 1 1 5DB7A883
P 8200 2200
F 0 "TP1" H 8325 2275 50  0000 C CNN
F 1 "TestPoint_Flag" H 8460 2203 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 8400 2200 50  0001 C CNN
F 3 "~" H 8400 2200 50  0001 C CNN
F 4 "-" H 0   0   50  0001 C CNN "MFR"
F 5 "-" H 0   0   50  0001 C CNN "MPN"
F 6 "-" H 0   0   50  0001 C CNN "SPR"
F 7 "-" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    8200 2200
	1    0    0    -1  
$EndComp
Text HLabel 3800 1300 0    50   Input ~ 0
RF_In
Text HLabel 4800 1300 2    50   Output ~ 0
RF_Out
Text Notes 3350 1600 0    50   ~ 0
20dB coupling
Text Notes 2700 3200 2    50   ~ 0
Port1: -3.1dB\nPort2: -3.3dB
Text HLabel 4100 2500 2    50   Output ~ 0
Monitor_Out
Text Notes 4700 1600 0    50   ~ 0
20dB coupling
Text Notes 4050 2400 0    50   ~ 0
-30dB monitoring out
Text HLabel 10100 2200 2    50   Output ~ 0
REV_CAL
Text HLabel 5800 5200 2    50   Output ~ 0
FWD_CAL
Text HLabel 5050 5100 2    50   Output ~ 0
FWD_OK
Text HLabel 5050 5000 2    50   Output ~ 0
FWD_ERR
Text HLabel 9400 2000 2    50   Output ~ 0
REV_ERR
Text HLabel 9400 2100 2    50   Output ~ 0
REV_OK
Text HLabel 5800 5400 2    50   Output ~ 0
FWD_POWER
Text HLabel 10100 2400 2    50   Output ~ 0
REV_POWER
Text HLabel 3800 5200 0    50   Input ~ 0
FWD_LIMIT
Text HLabel 8050 2200 0    50   Input ~ 0
REV_LIMIT
Text HLabel 8050 2100 0    50   Input ~ 0
RESET
Text HLabel 3800 5100 0    50   Input ~ 0
RESET
Wire Wire Line
	3800 1300 3900 1300
Wire Wire Line
	4800 1300 4700 1300
Wire Wire Line
	4300 1800 4300 1700
Wire Wire Line
	2600 2900 2750 2900
Wire Wire Line
	2750 2900 2750 2850
Wire Wire Line
	2750 2550 2750 2500
Wire Wire Line
	2750 2500 2600 2500
Wire Wire Line
	1800 2600 1700 2600
Wire Wire Line
	1700 2600 1700 2800
Wire Wire Line
	1700 2600 1250 2600
Wire Wire Line
	1250 2600 1250 2800
Wire Wire Line
	1250 3100 1250 3250
Wire Wire Line
	1250 3250 1700 3250
Wire Wire Line
	2000 3250 2000 3100
Wire Wire Line
	1700 3100 1700 3250
Wire Wire Line
	1700 3250 2000 3250
Wire Wire Line
	1800 2700 1600 2700
Wire Wire Line
	1600 1500 1600 2700
Wire Wire Line
	1600 1500 3900 1500
Wire Wire Line
	4550 4800 4550 4600
Wire Wire Line
	4550 4600 4650 4600
Wire Wire Line
	4650 4200 4550 4200
Wire Wire Line
	4550 4200 4550 4600
Wire Wire Line
	4550 4050 4550 4200
Wire Wire Line
	5050 4200 4950 4200
Wire Wire Line
	4950 4600 5050 4600
Wire Wire Line
	4050 4600 4550 4600
Wire Wire Line
	4150 5600 4000 5600
Wire Wire Line
	3650 5500 3650 5800
Wire Wire Line
	3650 5500 4150 5500
Wire Wire Line
	3650 6100 3650 6200
Wire Wire Line
	3650 6200 4000 6200
Wire Wire Line
	4000 5600 4000 6200
Wire Wire Line
	4000 6300 4000 6200
Wire Wire Line
	4000 6700 4000 6600
Wire Wire Line
	4000 7100 4000 7000
Wire Wire Line
	3350 5400 4150 5400
Wire Wire Line
	3050 5400 2900 5400
Wire Wire Line
	2900 5400 2900 5500
Wire Wire Line
	2900 5900 2900 5800
Wire Wire Line
	4050 4600 4050 5000
Wire Wire Line
	4050 5000 4150 5000
Wire Wire Line
	4950 5400 5050 5400
Wire Wire Line
	5050 5400 5050 5500
Wire Wire Line
	5050 5900 5050 5800
Wire Wire Line
	4550 5900 4550 5800
Wire Wire Line
	8900 1800 8900 1600
Wire Wire Line
	8900 1600 9000 1600
Wire Wire Line
	9000 1200 8900 1200
Wire Wire Line
	8900 1200 8900 1600
Wire Wire Line
	8900 1050 8900 1200
Wire Wire Line
	9400 1200 9300 1200
Wire Wire Line
	9300 1600 9400 1600
Wire Wire Line
	8400 1600 8900 1600
Wire Wire Line
	8500 2600 8350 2600
Wire Wire Line
	8000 2500 8000 2800
Wire Wire Line
	8000 2500 8500 2500
Wire Wire Line
	8000 3100 8000 3200
Wire Wire Line
	8000 3200 8350 3200
Wire Wire Line
	8350 2600 8350 3200
Wire Wire Line
	8350 3300 8350 3200
Wire Wire Line
	8350 3700 8350 3600
Wire Wire Line
	8350 4100 8350 4000
Wire Wire Line
	7650 2400 8500 2400
Wire Wire Line
	7350 2400 7250 2400
Wire Wire Line
	7250 2400 7250 2500
Wire Wire Line
	7250 2900 7250 2800
Wire Wire Line
	8400 1600 8400 2000
Wire Wire Line
	8400 2000 8500 2000
Wire Wire Line
	9300 2400 9400 2400
Wire Wire Line
	9400 2400 9400 2500
Wire Wire Line
	9400 2900 9400 2800
Wire Wire Line
	8900 2900 8900 2800
Wire Wire Line
	4000 2500 4100 2500
Wire Wire Line
	2750 2500 3200 2500
Wire Wire Line
	3400 3000 3400 2900
Wire Wire Line
	4700 1500 5800 1500
Wire Wire Line
	2800 5400 2900 5400
Wire Wire Line
	1900 4050 1900 5400
Wire Wire Line
	1900 5400 2000 5400
Wire Wire Line
	2750 2900 2750 4050
Wire Wire Line
	1900 4050 2750 4050
Wire Wire Line
	2200 5900 2200 5800
Wire Wire Line
	6600 1500 7250 1500
Wire Wire Line
	7250 1500 7250 2400
Wire Wire Line
	6000 2000 6000 1900
Wire Wire Line
	8050 2200 8200 2200
Wire Wire Line
	8050 2100 8500 2100
Wire Wire Line
	3800 5100 4150 5100
Wire Wire Line
	4150 5200 3850 5200
Wire Wire Line
	10100 2200 9800 2200
Wire Wire Line
	5800 5200 5500 5200
Wire Wire Line
	5050 5000 4950 5000
Wire Wire Line
	4950 5100 5050 5100
Wire Wire Line
	9300 2000 9400 2000
Wire Wire Line
	9400 2100 9300 2100
Wire Wire Line
	5800 5400 5500 5400
Wire Wire Line
	10100 2400 9800 2400
Wire Wire Line
	1700 3350 1700 3250
Wire Wire Line
	5500 5400 5050 5400
Wire Wire Line
	5500 5200 4950 5200
Wire Wire Line
	3850 5200 3800 5200
Wire Wire Line
	9800 2200 9300 2200
Wire Wire Line
	9800 2400 9400 2400
Wire Wire Line
	8200 2200 8500 2200
Connection ~ 1700 2600
Connection ~ 1700 3250
Connection ~ 4550 4600
Connection ~ 4550 4200
Connection ~ 4000 6200
Connection ~ 8900 1600
Connection ~ 8900 1200
Connection ~ 8350 3200
Connection ~ 2750 2500
Connection ~ 2900 5400
Connection ~ 2750 2900
Connection ~ 7250 2400
Connection ~ 5050 5400
Connection ~ 9400 2400
Connection ~ 5500 5400
Connection ~ 5500 5200
Connection ~ 3850 5200
Connection ~ 9800 2200
Connection ~ 9800 2400
Connection ~ 8200 2200
$EndSCHEMATC
