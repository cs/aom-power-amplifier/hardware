EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 9
Title "AOM Power Amplifier"
Date ""
Rev "1"
Comp "LMU"
Comment1 "Hendrik v. Raven"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L AOMPowerAmp-rescue:ERA-4XSM-RF_Amplifier U6
U 1 1 5D3AA3B0
P 5900 4050
F 0 "U6" H 5900 4417 50  0000 C CNN
F 1 "ERA-4XSM" H 5900 4326 50  0000 C CNN
F 2 "RF_Mini-Circuits:Mini-Circuits_WW107_LandPatternPL-075" H 5900 4050 50  0001 C CNN
F 3 "https://www.minicircuits.com/pdfs/ERA-4XSM+.pdf" H 5900 4050 50  0001 C CNN
F 4 "-" H 0   0   50  0001 C CNN "MFR"
F 5 "-" H 0   0   50  0001 C CNN "MPN"
F 6 "-" H 0   0   50  0001 C CNN "SPR"
F 7 "-" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    5900 4050
	1    0    0    -1  
$EndComp
$Comp
L AOMPowerAmp-rescue:ADCH-80-RF L?
U 1 1 5D3AB5B0
P 6800 3550
AR Path="/5D3AB5B0" Ref="L?"  Part="1" 
AR Path="/5D399AD6/5D3AB5B0" Ref="L1"  Part="1" 
F 0 "L1" V 6846 3420 50  0000 R CNN
F 1 "ADCH-80" V 6755 3420 50  0000 R CNN
F 2 "RF_Mini-Circuits:Mini-Circuits_CD542_H2.84mm" H 6800 3550 50  0001 C CNN
F 3 "https://ww2.minicircuits.com/pdfs/ADCH-80+.pdf" H 6800 3550 50  0001 C CNN
F 4 "-" H 0   0   50  0001 C CNN "MFR"
F 5 "-" H 0   0   50  0001 C CNN "MPN"
F 6 "-" H 0   0   50  0001 C CNN "SPR"
F 7 "-" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    6800 3550
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C11
U 1 1 5D3AC27A
P 7150 4050
F 0 "C11" V 6898 4050 50  0000 C CNN
F 1 "2n2" V 6989 4050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7188 3900 50  0001 C CNN
F 3 "~" H 7150 4050 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012205063" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "300-67-756" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    7150 4050
	0    1    1    0   
$EndComp
$Comp
L Device:C C9
U 1 1 5D3ACE51
P 7050 2500
F 0 "C9" V 6798 2500 50  0000 C CNN
F 1 "1u" V 6889 2500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7088 2350 50  0001 C CNN
F 3 "~" H 7050 2500 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012106022" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "300-67-640" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    7050 2500
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR038
U 1 1 5D3AD5A2
P 7300 2500
F 0 "#PWR038" H 7300 2250 50  0001 C CNN
F 1 "GND" V 7305 2372 50  0000 R CNN
F 2 "" H 7300 2500 50  0001 C CNN
F 3 "" H 7300 2500 50  0001 C CNN
	1    7300 2500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR040
U 1 1 5D3AD809
P 5900 4450
F 0 "#PWR040" H 5900 4200 50  0001 C CNN
F 1 "GND" H 5905 4277 50  0000 C CNN
F 2 "" H 5900 4450 50  0001 C CNN
F 3 "" H 5900 4450 50  0001 C CNN
	1    5900 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5D3ADEFD
P 6800 2900
F 0 "R11" H 6870 2946 50  0000 L CNN
F 1 "52.3 1%" H 6870 2855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6730 2900 50  0001 C CNN
F 3 "~" H 6800 2900 50  0001 C CNN
F 4 "Panasonic" H 0   0   50  0001 C CNN "MFR"
F 5 "ERJ3EKF52R3V" H 0   0   50  0001 C CNN "MPN"
F 6 "RS" H 0   0   50  0001 C CNN "SPR"
F 7 "122-9353" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    6800 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5D3AE8DD
P 5450 4050
F 0 "C10" V 5198 4050 50  0000 C CNN
F 1 "2n2" V 5289 4050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5488 3900 50  0001 C CNN
F 3 "~" H 5450 4050 50  0001 C CNN
F 4 "Wurth Elektronik" H 0   0   50  0001 C CNN "MFR"
F 5 "885012205063" H 0   0   50  0001 C CNN "MPN"
F 6 "Distrelec" H 0   0   50  0001 C CNN "SPR"
F 7 "300-67-756" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    5450 4050
	0    1    1    0   
$EndComp
$Comp
L power:+8V #PWR037
U 1 1 5D3EA045
P 6800 1600
F 0 "#PWR037" H 6800 1450 50  0001 C CNN
F 1 "+8V" H 6815 1773 50  0000 C CNN
F 2 "" H 6800 1600 50  0001 C CNN
F 3 "" H 6800 1600 50  0001 C CNN
	1    6800 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead FB1
U 1 1 5D6FFE93
P 6800 1900
F 0 "FB1" H 6937 1946 50  0000 L CNN
F 1 "BLM21BD" H 6937 1855 50  0000 L CNN
F 2 "Inductor_SMD:L_0805_2012Metric" V 6730 1900 50  0001 C CNN
F 3 "~" H 6800 1900 50  0001 C CNN
F 4 "-" H 0   0   50  0001 C CNN "MFR"
F 5 "-" H 0   0   50  0001 C CNN "MPN"
F 6 "-" H 0   0   50  0001 C CNN "SPR"
F 7 "-" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	1    6800 1900
	1    0    0    -1  
$EndComp
$Comp
L AOMPowerAmp-rescue:ADCH-80-RF L?
U 2 1 5D7A45CB
P 7750 3450
AR Path="/5D7A45CB" Ref="L?"  Part="2" 
AR Path="/5D399AD6/5D7A45CB" Ref="L1"  Part="2" 
F 0 "L1" H 7808 3599 50  0000 L CNN
F 1 "ADCH-80" H 7808 3508 50  0000 L CNN
F 2 "RF_Mini-Circuits:Mini-Circuits_CD542_H2.84mm" H 7750 3450 50  0001 C CNN
F 3 "https://ww2.minicircuits.com/pdfs/ADCH-80+.pdf" H 7750 3450 50  0001 C CNN
F 4 "-" H 0   0   50  0001 C CNN "MFR"
F 5 "-" H 0   0   50  0001 C CNN "MPN"
F 6 "-" H 0   0   50  0001 C CNN "SPR"
F 7 "-" H 0   0   50  0001 C CNN "SPN"
F 8 "-" H 0   0   50  0001 C CNN "SPURL"
	2    7750 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR039
U 1 1 5D7A5035
P 7750 3550
F 0 "#PWR039" H 7750 3300 50  0001 C CNN
F 1 "GND" H 7755 3377 50  0000 C CNN
F 2 "" H 7750 3550 50  0001 C CNN
F 3 "" H 7750 3550 50  0001 C CNN
	1    7750 3550
	1    0    0    -1  
$EndComp
Text Notes 7250 3000 0    50   ~ 0
bias resistance dependent on supply voltage\ncheck datasheet!\nadjusted for +8V
Text HLabel 7400 4050 2    50   Output ~ 0
RF_Out
Text HLabel 5200 4050 0    50   Input ~ 0
RF_In
Text Notes 6100 4450 0    50   ~ 0
amplifier can be switched for other models\nfrom the ERA series to increase gain
Wire Wire Line
	6800 3950 6800 4050
Wire Wire Line
	6800 4050 6300 4050
Wire Wire Line
	7000 4050 6800 4050
Wire Wire Line
	7200 2500 7300 2500
Wire Wire Line
	5900 4450 5900 4350
Wire Wire Line
	5600 4050 5700 4050
Wire Wire Line
	5200 4050 5300 4050
Wire Wire Line
	7300 4050 7400 4050
Wire Wire Line
	6800 3050 6800 3150
Wire Wire Line
	6800 2500 6900 2500
Wire Wire Line
	6800 2500 6800 2750
Wire Wire Line
	6800 2050 6800 2500
Wire Wire Line
	6800 1750 6800 1600
Wire Wire Line
	7750 3550 7750 3450
Connection ~ 6800 4050
Connection ~ 6800 2500
$EndSCHEMATC
