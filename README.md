# General
This repository contains the design files for the new AOM power amplifier
design developed at the Quantum Optics group of LMU. It uses a high power
amplifier, capable of up to 10W in the frequency range from 50 to 1000 MHz.
It is designed to the standard Eurocard dimensions of 220 by 100mm,
fitting in a box with 14TE.

In addition to the required pre-amplifiers it contains fast RF switches
and a mixer configured to control the RF intensity. It has been used
instead of a voltage variable attenuator to maintain compatability with
older designs (most notably the intensity stabilisation circuit).

## Parts
The design uses two PCBs. The main PCB houses the power supply, the full
RF path and all components to get the output runnig. The second PCB gets
stacked on top. It contains the extra components for the front panel which
are not required to run the amplifier (and would not fit on the main PCB).

# Orders
## PCBs
PCBs have been ordered via [Aisler](https://aisler.net). Direct links to
the projects:
- [Main PCB](https://aisler.net/p/RXMZPIEU)

## Components
All components should have their exact part number and a distributor added
in their schematic entries. You can see them when looking at the symbol
properties of each individual component. RS, Distrelec and Conrad have
been used where possible, most other components can be optained from
Mouser & Digi-Key. Some RF components need to be sourced from
Mini-Circuits or their distributors.
